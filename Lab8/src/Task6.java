
import java.util.HashMap;
import java.util.Map;

/*
 *Коллекция HashMap из Object Есть коллекция HashMap<String, Object>,
 *туда занесли 10 различных пар объектов. Вывести содержимое коллекции на экран,
 *каждый элемент с новой строки
 */

/**
 *
 * @author Администратор
 */
public class Task6 {
    public static void main(String[] args) {
        Map<String, Object> map = new HashMap<String, Object>();
        Object o1 = new Object();
        Object o2 = new Object();
        Object o3 = new Object();
        Object o4 = new Object();
        Object o5 = new Object();
        Object o6 = new Object();
        Object o7 = new Object();
        Object o8 = new Object();
        Object o9 = new Object();
        Object o10 = new Object();

        map.put("Арбуз", o1);
        map.put("Банан", o2);
        map.put("Вишня", o3);
        map.put("Груша", o4);
        map.put("Дыня", o5);
        map.put("Ежевика", o6);
        map.put("Жень-шень", o7);
        map.put("Земляника", o8);
        map.put("Ирис", o9);
        map.put("Картофель", o10);

        for (Map.Entry<String, Object> pair : map.entrySet()) {
            System.out.println(pair.getKey() + " - " + pair.getValue());
        }
    }
}
