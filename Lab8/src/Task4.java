/*
 * Вывести на экран список ключей Есть коллекция HashMap<String, String>,
 *туда занесли 10 различных строк. Вывести на экран список ключей,
 *каждый элемент с новой строки.
 */
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Администратор
 */
public class Task4 {

    public static void main(String[] args) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("Арбуз", "ягода");
        map.put("Банан", "трава");
        map.put("Вишня", "ягода");
        map.put("Груша", "фрукт");
        map.put("Дыня", "овощ");
        map.put("Ежевика", "куст");
        map.put("Жень-шень", "корень");
        map.put("Земляника", "ягода");
        map.put("Ирис", "цветок");
        map.put("Картофель", "клубень");

        for (Map.Entry<String, String> pair : map.entrySet()) {
            String key = pair.getKey();
            System.out.println(pair.getKey());
        }
    }
}
