
import java.util.HashMap;
import java.util.Map;

/*
 * Вывести на экран список значений Есть коллекция HashMap<String, String>,
 *туда занесли 10 различных строк. Вывести на экран список значений,
 *каждый элемент с новой строки.
 */
/**
 *
 * @author Администратор
 */
public class Task5 {

    public static void main(String[] args) {
        
        Map<String, String> map = new HashMap<String, String>();
        map.put("Арбуз", "ягода");
        map.put("Банан", "трава");
        map.put("Вишня", "ягода");
        map.put("Груша", "фрукт");
        map.put("Дыня", "овощ");
        map.put("Ежевика", "куст");
        map.put("Жень-шень", "корень");
        map.put("Земляника", "ягода");
        map.put("Ирис", "цветок");
        map.put("Картофель", "клубень");

        for (Map.Entry<String, String> pair : map.entrySet()) {
            System.out.println(pair.getValue());
        }
    }
}
